/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { Platform, StyleSheet, Image, View, Dimensions } from "react-native";
import Carousel, { ParallaxImage } from "react-native-snap-carousel";

const instructions = Platform.select({
  ios: "Press Cmd+R to reload,\n" + "Cmd+D or shake for dev menu",
  android:
    "Double tap R on your keyboard to reload,\n" +
    "Shake or press menu button for dev menu"
});

type Props = {};

const sliderWidth = Dimensions.get('window').width;
const itemHeight = Dimensions.get('window').height;

export default class App extends Component<Props> {
  constructor() {
    super();
    this.state = {
      pages: [
        require("./images/p1.jpg"),
        require("./images/p2.jpeg"),
        require("./images/p3.jpg")
      ]
    };
  }

  _renderItem({ item, index }, parallaxProps) {
    return (
      <View style={styles.item}>
        <Image
          resizeMode={"cover"}
          resizeMethod={"scale"}
          source={item}
          containerStyle={styles.imageContainer}
          style={styles.imageContainer}
          parallaxFactor={0.4}
          {...parallaxProps}
        />
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <Carousel
          sliderWidth={sliderWidth}
          itemWidth={sliderWidth * 0.8}
          itemHeight={itemHeight}
          data={this.state.pages}
          renderItem={this._renderItem}
          hasParallaxImages={true}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
